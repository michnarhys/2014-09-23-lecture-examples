#import class modules
import circle_class as cc
import tri_class as tc
#instantiate a circle and a triangle object
circ = cc.Circle()
tri = tc.Triangle()
#intitialize attributes
tri.height = 1
tri.width = 2
circ.radius = 2

#compute and print areas
print 'circle area is ', circ.getArea()
print 'triangle area is ', tri.getArea()
